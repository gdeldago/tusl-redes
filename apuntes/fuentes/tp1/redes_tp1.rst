.. header:: 
  Redes - Trabajo Práctico 1 - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. raw:: pdf

    Spacer 0 200
	PageBreak oneColumn

.. contents:: Contenido

.. raw:: pdf

    Spacer 0 200
	PageBreak 

Copyright©2019.

:Autor:       Bárbara Carina Yunges / Gustavo Del Dago

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: ../imagenes/licencia.png
	:scale: 100%

.. raw:: pdf

    PageBreak 


Trabajo Práctico 1.
===================

Consigna de trabajo.
--------------------

Elaborar un documento que registre las actividades relizadas, los resultados obtenidos, 
los eventuales inconvenientes, las respuestas a las preguntas concretas y las reflexiones 
personales sobre la experiencia.

Para las actividades desarrolladas en el entorno de simulación de redes (*Networksimulator*) 
se solicita el envío del archivo con el modelo correspondiente (extensión *.json*). 

IP (Internet Protocol).
-----------------------

Abrir una consola (Ctrl + Alt + T) y responder:

1. Liste las interfaces de red activas de su computadora.  
    a. Mostrar captura de pantalla.
    b. ¿Cuáles interfaces encontró?
    c. ¿Qué comando utilizó?

#. En la siguiente interfaz:
    .. image:: imagenes/interfaz.png
	    :scale: 240%
   
    a. ¿A qué tipo de conexión pertenece la interfaz (Ethernet, Wi-Fi, etc.)?
    b. Identifique la dirección IP. 
    c. Identifique la máscara de subred.
    d. ¿Cuál es la IP de la red? ¿Cómo la calculó?
    e. ¿Qué son “RX packets” y “TX packets”?. ¿Qué significado tiene que su valor sea cero?  

#. Utilizando el simulador de redes *NetworkSimulator*, elaborar un modelo de red local con las siguientes característica:
    a. Topología estrella.
    b. Por tratarse de una red de área local (*LAN*) se solitica que el espacio de direcciones IP sea privado.
    c. Deben conectarse por lo menos 3 equipos.
    d. Se desea disponer de asignación dinámica de direcciones IP.

#. Una vez elaborado el modelo se solicita:
    a. Indicar los pasos necesarios para que los equipos obtengan su dirección IP.
    b. Probar la conexión entre los nodos.


ARP (Address Resolution Protocol).
----------------------------------

Antes de comenzar:

* En la red LAN donde se va a realizar el trabajo conectar mínimamente dos dispositivos (ej, PC + celular).
* Instalar el programa *Wireshark* y abrirlo como usuario root. Ejemplo en ubuntu:

        .. code:: bash

            $ apt-get install wireshark 


        .. code:: bash  

            $ sudo wireshark

1. Desde la consola (Ctrl + Alt + T): 
    a. Listar las direcciones MAC de las interfaces de su PC. 
    b. ¿Qué comando utilizó?.

#. Observar la tabla ARP de la PC. 
    a. Mostrar captura de pantalla. 
    b. ¿Qué comando utilizó?.

#. *Wireshark* es un analizador de protocolos utilizado para realizar análisis y solucionar problemas en redes de comunicaciones. La primera pantalla de *Wireshark* presenta una lista de interfaces. 
    a. ¿Qué relaciones podemos establecer entre los elementos de esta lista y la salida del comando ifconfig?.

    * Nosotros utilizaremos  *Wireshark* para estudiar algunas cuestiones relacionadas con la resolución de direcciones. Para ello nos valdremos de una función que permite capturar mensajes (paquetes). 
    * El primer paso es indicar cuál es la interfaz que nos interesa analizar. Bastará con seleccionar un elemento de la lista. Si bien es posible seleccionar más de una interfaz de manera simultánea, nosotros comenzaremos en un escenario más sencillo (analizando paquetes de una sola interfaz).
    * Sabemos que la cantidad de paquetes que se transmiten por una interfaz puede ser muy grande. Para acotar la captura de paquetes se pueden emplear filtros. En nuestro caso aplicaremos un filtro que indica el protocolo al que pertenecen los paquetes que nos interesan.
    * Con las dos elecciones anteriores (interfaz y protocolo) habremos restringido la cantidad de paquetes que *WireShark* estará capturando y que nos servirán para la posterior etapa de análisis.
    * Aplicar ARP en el filtro, para observar solo los paquetes de ese protocolo.
    * Ya estamos en condiciones de iniciar la captura de paquetes presionando el icono de “aleta de tiburón”.

.. image:: imagenes/wireshark.png
    :scale: 130%


Ahora nos ocuparemos de generar algo de tráfico en la red.
 
#. Borrar la caché ARP de la pc con el comando:sudo ip -s -s neigh flush all. 
    a. Mostrar captura de pantalla.

#. Detener la captura de paquetes en *Wireshark*. 
    a. Ver paquetes capturados. Mostrar captura de pantalla como mínimo de los primeros 10 paquetes.

#. Observar nuevamente la tabla ARP. 
    a. Mostrar captura de pantalla. 

#. En *Wireshark* ir al menú “Statistics” , seleccionar “Flow graph”. 
    a. Nos mostrará un diagrama de flujo en el que podremos observar la comunicación entre los equipos.
    b. En la parte superior podremos observar las IPs o MACs de los dispositivos con los que hemos intercambiado paquetes.
    c. ¿Qué podemos decir de las direcciones MAC que nos muestra *WireShark*? Pista: ¿Qué representan los 3 primeros bloques de 8 bits de una dirección MAC?.
    d. Capturar la pantalla y marcar un paquete del tipo broadcast con su respuesta.

#. Abrir un paquete del tipo Broadcast. Capturar su contenido. Indicar:
    a. Destino.
    b. ¿Qué significa FF:FF:FF:FF:FF:FF?.
    c. Origen.
    d. ¿Cuál es la finalidad de este tipo de paquetes?.

#. Abrir un paquete de respuesta al de Broadcast. Indicar:
    a. Destino.
    b. Origen.
    c. ¿Qué significan las siguientes 4 líneas de este paquete?: 
        1. Sender MAC address
        2. Sender IP address
        3. Target MAC address y
        4. Target IP address.
    d. ¿Cuál es la finalidad de este tipo de paquetes?.



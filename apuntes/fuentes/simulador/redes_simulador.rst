.. header:: 
  Redes - Simulador de redes - Breve introducción a `Network Simulator` - Página ###Page### de ###Total###

.. footer::
  TECNICATURA UNIVERSITARIA EN SOFTWARE LIBRE - FICH-UNL 

.. raw:: pdf

    Spacer 0 200
	PageBreak oneColumn

.. contents:: Contenido

.. raw:: pdf

    Spacer 0 200
	PageBreak 

Copyright©2019.

:Autor:       Gustavo Del Dago

.. rubric:: ¡Copia este texto!

Los textos que componen este trabajo se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas, siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.

Este trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: ../imagenes/licencia.png
	:scale: 60%

.. raw:: pdf

   PageBreak 



Breve introducción al simulador de redes *Network Simulator*.
=============================================================

¿Qué es un simulador de redes?
------------------------------

Los ambientes de simulación de redes nos ofrecen un entorno que facilita y promueve la experimentación. Normalmente se trata de modelos dinámicos sobre los que podremos aplicar estímulos y observar comportamientos. 

Existe una diversidad de herramientas de simulación de uso libre. La mayoría de los programas disponibles ofrecen funciones de modelado y diagnóstico adecuadas para estudiar problemas y diseñar redes de cierta complejidad. 

El dominio de dichas herramientas requiere un entrenamiento que, si bien no deja de ser algo meramente técnico e instrumental, podría desalentar a quienes están dando sus primeros pasos en el mundo de las redes de computadoras. 

Simulador de redes *Network Simulator*.
---------------------------------------

El programa que aquí presentaremos, *NetworkSimulator*, es un simulador de redes desarrollado con fines educativos. Se trata de un programa sencillo que permite explorar los conceptos fundamentales de las redes de computadoras y de la familia de protocolos TCP/IP.

Creemos que la experiencia adquirida en el ambiente de simulación de redes *NetworkSimulator* será valiosa tanto para afianzar los conceptos teóricos fundamentales cómo para desarrollar las habilidades necesarias que faciliten el abordaje de herramientas de simulación de mayor complejidad.
 
Guía de actividades.
--------------------
A continuación presentamos una guía de actividades con pistas y claves de resolución que ilustran un posible modo de uso del simulador de redes (en adelante usaremos simplemente simulador). 
La intención es mostrar algunas características relacionadas con la interfaz de usuario de la herramienta mediante ejemplos concretos. Así, quienes tengan experiencia con otros entornos de simulación podrán apreciar la lógica de trabajo que propone NetworkSimulator y quienes estén dando sus primeros pasos dispondrán de una guía que los oriente. Cómo se verá, las cuestiones relacionadas con el manejo de la herramienta no representan mayor complejidad. Lo anterior es importante porque nos permite, una vez conocidos los mecanismos para movernos dentro del entorno de simulación, concentrarnos en lo realmente importante: los conceptos fundamentales de las redes de datos y del conjunto de protocolos TCP/IP. 

Acceder al simulador.
.....................

*Network Simulator* se encuentra disponible en `Simulador de Redes <https://gdeldago.gitlab.io/tusl-redes/>`_.

Si se quiere obtener una copia, por ejemplo para trabajar de modo local (sin necesidad de conexión a Internet), se puede acceder al `Repositorio del proyecto <https://gitlab.com/gdeldago/tusl-redes>`_. 

El entorno de trabajo.
......................

En la interfaz de usuario del simulador encontramos tres elementos principales: El *área de trabajo* que ocupa toda la pantalla, el *menú de opciones* que aparece en la esquina superior izquierda y el *menú de animación* inicialmente centrado en el borde superior. A los anteriores se sumará, como veremos más adelante, el *menú contextual*.

La siguiente imagen muestra el entorno de trabajo con el *menú de opciones* desplegado.


.. image:: imagenes/imagen_00.png
		:scale: 200%


Para incorporar elementos al modelo bastará con seleccionar la opción correspondiente de menú. Todas las opciones comienzan con el termino “Añadir”.  

Cuando nos interese guardar los modelos elaborados para continuar trabajando en futuras sesiones o para compartilos con otras personas podremos hacerlo utilizando la opción *Guardar*. Mediante esta opción obtendremos un archivo en formato *json*. Tengamos presente la conveniencia de emplear nombres claros y significativos para los recursos digitales.

Para recuperar un modelo a partir del archivo *json* correspondiente debemos utilizar la opción *Gargar*.

*La red más sencilla del mundo*.
................................

Vamos a elaborar un modelo para *la red más sencilla del mundo*: una en la que sólo participan dos nodos. Para conseguirlo vamos a realizar tres pasos: 

1. Añadir los equipos
2. Establecer un enlace entre los mismos y 
3. Asignar una direcciones IP única a cada nodo.

Paso 1: Añadir *equipos*.
~~~~~~~~~~~~~~~~~~~~~~~~~
Cada vez que se agregue un componente al modelo, el simulador le asignará de manera automática un nombre que luego podrá ser modificado de acuerdo a nuestras necesidades. Para ello debemos utilizar el menú contextual. 


.. image:: imagenes/imagen_01.png
		:scale: 200%


El acceso al *menú contextual* requiere que el componente se encuentre seleccionado. En la imagen puede verse un equipo con su *menú contextual* seleccionado.


.. image:: imagenes/imagen_02.png
		:scale: 200%


La opción *cambiar nombre/grupo* presenta una caja de diálogo desde la que podremos modificar tanto el nombre del equipo como el del grupo al que éste pertenece. 
Durante esta actividad llamaremos a los equipos con los nombres “nodo1.minired” y “nodo2.minired” para indicar que se trata de nodos de la “minired”. 
En el *área de trabajo* los equipos muestran sus direcciones MAC e IP (aunque la dirección IP no haya sido asignada).

Paso 2. Establecer enlaces.
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Una vez que contemos con dos nodos, estaremos en condiciones de crear *la red más sencilla del mundo*. Para ello será suficiente crear un enlace entre ambos nodos. 

Los *enlaces* se crean utilizando la opción *Crear enlace* desde el *menú contextual* de cualquiera de los componentes entre los que se pretenda establecer el vínculo. Una vez seleccionado, se mostrará una línea de enlace cuyo extremo libre nos permitirá seleccionar el segundo componente participante del enlace. El último paso será seleccionar la *interfaz de red* de ambos componentes. En el simulador los equipos tienen sólo una *interfaz de red*, los enrutadores (*router*) y los conmutadores (*switch*) tienen ocho *interfaces de red*.

En la imagen se puede ver la caja de diálogo de la operación crear enlace.


.. image:: imagenes/imagen_02.png
		:scale: 200%


Paso 3. Establecer direcciones IP.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La dirección IP de un nodo puede asignarse mediante la opción *Editar info. IP – Interfaz 0* del *menú contextual* correspondiente. El simulador presentará una caja de diálogo desde donde se podrán configurar: la dirección IP, la máscara de red y los servidores DNS.


.. image:: imagenes/imagen_04.png
		:scale: 200%


En nuestra actividad emplearemos las direcciones 192.168.1.100 y 192.168.1.101 que corresponden al espacio privado de direcciones IPv4 clase C. En la siguiente figura puede verse el modelo terminado.


.. image:: imagenes/imagen_05.png
		:scale: 200%


Herramientas de diagnóstico.
............................

El simulador ofrece algunas herramientas de diagnóstico que serán de gran ayuda a la hora de estudiar el modelo en funcionamiento. Dichas herramientas convierten al modelo en un ambiente dinámico donde podremos enviar mensajes entre los nodos y observar los efectos y resultados.

En esta actividad utilizaremos el comando *ping* a fines de observar el intercambio de mensajes entre los nodos de nuestra red.

Al tratarse de un comando que se emite desde un nodo, debemos utilizar el *menú contextual* del nodo emisor. La opción del menú es *Diagnósticos de red*. Al seleccionar dicha opción se presenta una caja de diálogo donde podremos indicar el nodo destino utilizando el campo *Interfaz 0* y el comando correspondiente a la herramienta: *ping* o *Trace route*. El resultado de la operación se registrará en la misma caja de diálogo de las herramientas de diagnóstico. Al mismo tiempo, se podrá ver el intercambio de mensajes entre los nodos. 
Los controles de animación permiten ajustar la velocidad de la animación, detenerla y reanudarla. Es una herramienta útil cuando se quiera visualizar las direcciones de origen y destino de cada mensaje. En la imagen puede verse el mensaje enviado por el nodo 192.168.1.100


.. image:: imagenes/imagen_06.png
		:scale: 200%


Asignación dinámica de direcciones.
...................................

La asignación de direcciones dinámicas puede resolverse añadiendo un *Servidor DHCP*. 
Agregar un nodo en nuestra red requiere cambiar su *topología* ya que un *enlace punto a punto* no admite, por definición, más de dos participantes. 

Incluir un conmutador (*switch*) habilita la creación de una red con *topología de estrella*. Antes de configurar el *servidor DHCP*, vamos a eliminar la configuración de las direcciones IP de los equipos. La imagen muestra la red resultante, nótese que ninguno de los equipos tiene asignada dirección IP.

  
.. image:: imagenes/imagen_07.png
		:scale: 200%


La configuración del *servidor DHCP*, a la que se accede desde el *menú contextual*, permite definir el rango de direcciones (campos *Inicial* y *Final*), la puerta de enlace predeterminada (campo *Gateway*) y dos servidores DNS (campos *DNS1* y *DNS2*). Para atender mensajes de solicitud, el *servidor DHCP* deberá tener configurada su dirección IP. Nosotros asignaremos la dirección IP 192.168.1.80

Cuando se requiera la asignación automática de dirección IP se deberá enviar el mensaje *Solicitar info. DHCP* desde el menú contextual del nodo correspondiente.
Como se puede ver en la siguiente imagen, los mensajes de *solicitud DHCP* son enviados a todos los nodos de la red (técnica denominada “broadcast”).


.. image:: imagenes/imagen_08.png
		:scale: 200%


El *servidor DHCP*, siempre que esté configurado, despacha un mensaje de respuesta con la dirección IP asignada al nodo solicitante.


.. image:: imagenes/imagen_09.png
		:scale: 200%





